import React, { Component } from "react";
import { View, Text, ImageBackground, TextInput, ScrollView, TouchableOpacity } from "react-native";
import { NavigationInjectedProps } from "react-navigation";
import { TextField } from "../../../components/TextField";
import { validateSignUpForm, showMessagePopUp } from "../../../resources/services";
import styles from "./styles";

import BackgroundImage from "../../../../assets/tollBackground.jpg";

class SignUpForm extends React.Component<NavigationInjectedProps> {
	state = {
		firstName: "",
		lastName: "",
		email: "",
		password: "",
		confirmPassword: "",
		vehicleNumber: "",
		vehicleType: ""
	};

	handleFirstNameChange = (firstName: string) => {
		this.setState({ firstName });
	};

	handleLastNameChange = (lastName: string) => {
		this.setState({ lastName });
	};

	handleEmailChange = (email: string) => {
		this.setState({ email });
	};

	handleVehicleTypeChange = (vehicleType: string) => {
		this.setState({ vehicleType });
	};

	handleVehicleNumberChange = (vehicleNumber: string) => {
		this.setState({ vehicleNumber });
	};

	onChangePassword = (text: string) => {
		this.setState({ password: text });
	};

	onChangeConfirmPassword = (text: string) => {
		this.setState({ confirmPassword: text });
	};

	render() {
		const {
			firstName,
			lastName,
			email,
			password,
			confirmPassword,
			vehicleType,
			vehicleNumber
		} = this.state;
		return <React.Fragment>{this.renderSignUpContent()}</React.Fragment>;
	}

	renderSignUpContent = () => {
		return (
			<ImageBackground source={BackgroundImage} style={styles.backgroundStyle}>
				<ScrollView style={styles.scrollStyle}>
					<View style={styles.viewStyle}>
						<View style={styles.innerView}>
							<Text />
							<TextField
								placeholder="First name"
								keyboardType="default"
								onChangeText={this.handleFirstNameChange}
								// value={firstName}
								style={styles.inputStyle}
								// iconName="mail"
							/>
							<Text />
							<TextField
								placeholder="Last Name"
								keyboardType="default"
								onChangeText={this.handleLastNameChange}
								// value={firstName}
								style={styles.inputStyle}
								// iconName="mail"
							/>
							<Text />
							<TextField
								placeholder="Email"
								keyboardType="default"
								onChangeText={this.handleEmailChange}
								// value={firstName}
								style={styles.inputStyle}
								iconName="user"
							/>
							<Text />

							<TextField
								placeholder="Password"
								keyboardType="default"
								onChangeText={this.onChangePassword}
								// value={firstName}
								style={styles.inputStyle}
								iconName="user"
							/>
							<Text />
							<TextField
								placeholder="Confirm Password"
								keyboardType="default"
								onChangeText={this.onChangeConfirmPassword}
								// value={firstName}
								style={styles.inputStyle}
								iconName="user"
							/>
							<Text />
							<TextField
								placeholder="Vehicle Type"
								keyboardType="default"
								onChangeText={this.handleVehicleTypeChange}
								// value={firstName}
								style={styles.inputStyle}
								iconName="user"
							/>
							<Text />
							<TextField
								placeholder="Vehicle Number "
								keyboardType="default"
								onChangeText={this.handleVehicleNumberChange}
								// value={firstName}
								style={styles.inputStyle}
								iconName="user"
							/>
							<Text />
						</View>
						<TouchableOpacity style={styles.button} onPress={this.onSignUp}>
							<Text style={styles.extraTextStyle}> SIGN UP </Text>
						</TouchableOpacity>
					</View>
				</ScrollView>
			</ImageBackground>
		);
	};

	onSignUp = () => {
		this.props.navigation.navigate("MainStack");
	};

	// onSignUp = async () => {
	// 	const validForm = validateSignUpForm(this.state);
	// 	if (!validForm.status && validForm.error) {
	// 		showMessagePopUp(this.state.email);
	// 		showMessagePopUp(validForm.error.message);
	// 		return;
	// 	}
	// 	this.props.navigation.navigate("MainStack");
	// };
}

export default SignUpForm;
